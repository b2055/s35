//JSON web tokens are standard for sending info between our application in a secure manner
//will allows us to gain access to methods that will help us to create a JSON web token

const jwt = require('jsonwebtoken')
const secret = "CrushAkoNgCrushKo"

//JWT is a way of securely passing info from the server to the frontend or to the other parts of server
//info is kept secure through the use of the secret code
//only the system that knows the secret code that can decode the encrypted info.


//Token Creation
//Analogy: Pack the gift and provide a lock with the secret code as the key

module.exports.createAccessToken = (user) => {
    //Data will be received from the registration form
    //When the users log in, a token will be created with user's information

    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

        //generates the token using the form data and secret code
    return jwt.sign(data, secret, {})
}

//Token verification

module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization

    if(typeof token !== "underfined") {
        console.log(token)

        //Bearer
        token = token.slice(7, token.length)

        //validate the token using the "verify" method
        return jwt.verify(token, secret, (err, data) => {
            //if jwt is not valid
            if(err) {
                return res.send({auth: "failed"})
            } else {
                next()
            }
        })
    }

    // Token does not exist

    else {
        return res.send({auth: "failed"})
    }
    
}

module.exports.decode = (token) => {
    //token recieved and is not undefiend
    if(typeof token !== "undefined") {
        token = token.slice(7, token.length)

        return jwt.verify(token,secret, (err, data) => {
            if(err){
                return null
            } else {
                // "decode" method is used to obtain information from the JWT
                // "{complete:true" option allows to return additional information from the JWT
                // Returns an object with access to the "payload" property which contains user information stored when the token was generated

                return jwt.decode(token, {complete:true}).payload
            }
        })
    } else {
        return null
    }
}