const Course = require('../models/Course')
const auth = require('../auth.js')

module.exports.addCourse = (reqBody, userData) => {

    return Course.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return "You are not an admin"
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newCourse.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Course creation successful"
                }
            })
        }
        
    });    
}

//retrieve all courses
module.exports.getAllCourse = () => {
    return Course.find({}).then(result => {
        return result
    })
}

//retrieve all active courses
module.exports.getAllActive = () => {
    return Course.find({isActive : true}).then(result => {
        return result
    })
}

// Retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}


//Update a course

module.exports.updateCourse = (reqParams, reqBody) => {
    let updatedCourse = {
        name: reqBody.name,
        description:reqBody.description,
        price: reqBody.price
    }

    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
        if(error) {
            return false
        } else {
            return true
        }
    })
}

//Archive a course
module.exports.archiveCourse = (reqParams, reqBody) => {
    let updatedCourse = {
        isActive: false
    }

    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
        if(error) {
            return false
        } else {
            return course
        }
    })
}