const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController.js')
const auth = require('../auth.js')

//Route for checking if the user's email already exists in the database
router.post('/checkEmail', (req, res) => {
    userController.checkEmailExists(req.body).then(result => res.send(result))
})

//Routes for user registration
router.post('/register', (req, res) => {
    userController.registerUser(req.body).then(result => res.send(result))
})

//Routes for authenticating a user
router.post('/login', (req, res) => {
    userController.loginUser(req.body).then(result => res.send(result))
})

//Routes for retrieving details of the user
router.get('/details', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    //provides the user's ID for the getPRofile controller method
    userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController))
})

module.exports = router;